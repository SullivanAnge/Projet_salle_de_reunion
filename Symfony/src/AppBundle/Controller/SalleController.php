<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reservation;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use AppBundle\Entity\Salle;
use Nelmio\ApiDocBundle\Annotation\Model;
use Unirest\Request as Uni;

class SalleController extends Controller
{
    /**
     * @Route("/salles/{token}", name="salles_list")
     * @Method({"GET"})
     * * @SWG\Response(
     *     response=200,
     *     description="Retourne la liste de toutes les salles",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Tag(name="Salles")
    */
    public function getSallesAction(Request $request)
    {

        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $users = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Salle')
            ->findAll();

        $formatted = [];
        foreach ($users as $user) {
            $formatted[] = [
                'id' => $user->get_id(),
                'num_salle' => $user->get_num_salle(),
                'nom_salle' => $user->get_nom_salle()
            ];
        }
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/salles/{token}/{id}", name="get_salle")
     * @Method({"GET"})
     * * @SWG\Response(
     *     response=200,
     *     description="Retourne les infos d'une salle",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * * @SWG\Response(
     *     response=404,
     *     description="La salle n'existe pas",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="ID de la salle"
     * )
     *
     * @SWG\Tag(name="Salles")
     */
    public function getSalleAction(Request $request)
    {
        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $salle = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Salle')
            ->find($request->get('id'));

            if($salle == null){
                return new JsonResponse('La salle n\'existe pas', 404);
            }

            $formatted = [
                'id' => $salle->get_id(),
                'num' => $salle->get_num_salle(),
                'nom' => $salle->get_nom_salle(),
            ];
        return new JsonResponse($formatted);
    }
    /**
     * @Route("/dispo/{token}/{heure_debut}/{heure_fin}/{date}", name="get_dispo")
     * @Method({"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Affiche les salles disponibles pour une tranche d'horaire donnée",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Parameter(
     *     name="heure_debut",
     *     in="path",
     *     type="string",
     *     description="Heure de début au format HH:MM:SS"
     * )
     * @SWG\Parameter(
     *     name="heure_fin",
     *     in="path",
     *     type="string",
     *     description="Heure de fin au format HH:MM:SS"
     * )
     * @SWG\Parameter(
     *     name="date",
     *     in="path",
     *     type="string",
     *     description="Date au format AAAA-MM-DD"
     * )
     *
     * @SWG\Tag(name="Salles")
     */

    public function getDispoAction(Request $request)
    {
        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $heure_debut = substr($request->get('heure_debut'), 0,2);
        $heure_debut = (int)$heure_debut;
        $heure_fin = substr($request->get('heure_fin'), 0,2);
        $heure_fin = (int)$heure_fin;
        $date = $request->get('date');

        $salles = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Salle')
            ->findAll();

        $sql = "
            SELECT * from salle s
            WHERE NOT EXISTS(
                SELECT * from reservation WHERE s.id = id_salle AND
                date = '$date' AND
                (
                    (
                        heure_debut <= $heure_debut AND
                        heure_fin > $heure_debut
                    ) OR (
                        heure_debut < $heure_fin AND
                        heure_fin >= $heure_fin
                    )
                )
            )
        ";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $formatted = $stmt->fetchAll();

        return new JsonResponse($formatted);
    }

    /**
     * @Route("/reserver/{token}/{id_salle}/{heure_debut}/{nb_heures}/{id_user}/{date}", name="post_salle")
     * @Method({"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="Ajoute une reservation",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * * @SWG\Response(
     *     response=404,
     *     description="La salle n'existe pas",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Parameter(
     *     name="id_salle",
     *     in="path",
     *     type="integer",
     *     description="Id de la salle"
     * )
     * @SWG\Parameter(
     *     name="heure_debut",
     *     in="path",
     *     type="string",
     *     description="Heure de début au format HH:MM:SS"
     * )
     * @SWG\Parameter(
     *     name="nb_heures",
     *     in="path",
     *     type="integer",
     *     description="Nombre d'heures à réserver"
     * )
     * @SWG\Parameter(
     *     name="id_user",
     *     in="path",
     *     type="integer",
     *     description="Id de l'utilisateur"
     * )
     *
     * @SWG\Parameter(
     *     name="date",
     *     in="path",
     *     type="string",
     *     description="Date de réservation"
     * )
     *
     * @SWG\Tag(name="Salles")
     */

    public function postReservationAction(Request $request)
    {
        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $heure_debut = substr($request->get('heure_debut'), 0,2);
        $nb_creneau = $request->get('nb_heures');
        $heure_fin = $heure_debut+$nb_creneau;
        $id_user = $request->get('id_user');
        $id_salle = $request->get('id_salle');
        $date = $request->get('date');
        $date_formatted =date_create($request->get('date'));

        $salle = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Salle')
            ->find($request->get('id_salle'));

        if($salle == null){
            return new JsonResponse('La salle n\'existe pas', 404);
        }

        $sql = "
                SELECT * from reservation
                WHERE id_salle = $id_salle AND
                date = '$date' AND (
                    (
                        heure_debut <= $heure_debut AND
                        heure_fin > $heure_debut
                    ) OR (
                        heure_debut < $heure_fin AND
                        heure_fin >= $heure_fin
                    )
                )
        ";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $req = $stmt->fetchAll();

        if (empty($req)) {
            $em = $this->getDoctrine()->getManager();

            $reservation = new Reservation();
            $reservation->set_user($id_user);
            $reservation->set_salle($id_salle);
            $reservation->set_date($date_formatted);
            $reservation->set_heure_debut($heure_debut);
            $reservation->set_heure_fin($heure_fin);

            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($reservation);

            // actually executes the queries (i.e. the INSERT query)
            $res = $em->flush();

            $formatted = array (
                'res' => 'success',
                'msg' => 'Réservation effectuée'
            );
        }else{
            $formatted = array(
                'res' => 'err',
                'msg' => 'Il y a déjà une réservation à cette heure ci'
            );
        }
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/planning/{token}/{id}", name="get_salle_planning")
     * @Method({"GET"})
     * * @SWG\Response(
     *     response=200,
     *     description="Retourne le planning d'une salle",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * * @SWG\Response(
     *     response=404,
     *     description="La salle n'existe pas",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="ID de la salle"
     * )
     *
     * @SWG\Tag(name="Salles")
     */
    public function getSallePlanningAction(Request $request)
    {
        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $salle = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Salle')
            ->find($request->get('id'));

        if($salle == null){
            return new JsonResponse('La salle n\'existe pas', 404);
        }

        $reservations = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Reservation')
            ->findBy(array('id_salle' => $request->get('id')));

        $formatted = [
            'id' => $salle->get_id(),
            'num' => $salle->get_num_salle(),
            'nom' => $salle->get_nom_salle()
        ];

        $planning= [];

        foreach ($reservations as $reservation) {
            $response = Uni::get('http://chezmeme.com/sso/api/v1.2/users/'.$request->get('token').'/'.$reservation->get_user(), $headers);
            $tab = $response->body;

            $planning[] = [
                'id' => $reservation->get_id(),
                'nom' => $tab[0]->nom,
                'prenom' => $tab[0]->prenom,
                'date' => $reservation->get_date(),
                'heure_debut' => $reservation->get_heure_debut(),
                'heure_fin' => $reservation->get_heure_fin(),
            ];
        }

        $formatted['planning']=$planning;

        return new JsonResponse($formatted);
    }

    /**
     * @Route("/reservation/{token}/{id}/delete", name="delete_reservation")
     * @Method({"DELETE"})
     * * @SWG\Response(
     *     response=200,
     *     description="Supprime une reservation",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * * @SWG\Response(
     *     response=404,
     *     description="La salle n'existe pas",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="ID de la réservation"
     * )
     *
     * @SWG\Tag(name="Salles")
     */
    public function deleteReservationAction(Request $request)
    {
        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $em = $this->get('doctrine.orm.entity_manager');

        $reservation = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Reservation')
            ->find($request->get('id'));


        if($reservation == null){
            return new JsonResponse('La salle n\'existe pas', 404);
        }

        $em->remove($reservation);
        $em->flush();

        $formatted = [
            'La salle a bien été supprimée'
        ];

        return new JsonResponse($formatted);
    }

    /**
     * @Route("/reserver/edit/{token}/{id}/{heure_debut}/{nb_heures}/{date}", name="put_salle")
     * @Method({"PUT"})
     * @SWG\Response(
     *     response=200,
     *     description="Ajoute une reservation",
     * )
     * * @SWG\Response(
     *     response=403,
     *     description="Accès non autorisé",
     * )
     * * @SWG\Response(
     *     response=404,
     *     description="La salle n'existe pas",
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="path",
     *     type="string",
     *     description="Token utilisateur"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Id reservation"
     * )
     * @SWG\Parameter(
     *     name="heure_debut",
     *     in="path",
     *     type="string",
     *     description="Heure de début au format HH:MM:SS"
     * )
     * @SWG\Parameter(
     *     name="nb_heures",
     *     in="path",
     *     type="integer",
     *     description="Nombre d'heures à réserver"
     * )
     *
     * @SWG\Parameter(
     *     name="date",
     *     in="path",
     *     type="string",
     *     description="Date de réservation"
     * )
     *
     * @SWG\Tag(name="Salles")
     */

    public function updateReservationAction(Request $request)
    {
        $headers = array('Accept' => 'application/json');
        $response = Uni::get('http://chezmeme.com/sso/api/v1.4/check_access/'.$request->get('token').'/salles', $headers);
        $res = $response->body->ret; //Resultat
        if($res != "granted"){
            return new JsonResponse('Accès refusé', 403);
        }

        $heure_debut = substr($request->get('heure_debut'), 0,2);
        $nb_creneau = $request->get('nb_heures');
        $heure_fin = $heure_debut+$nb_creneau;
        $date = $request->get('date');
        $date_formatted=date_create($request->get('date'));
        $id = $request->get('id');

        $reservation = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Reservation')
            ->find($request->get('id'));

        if($reservation == null){
            return new JsonResponse('La réservation n\'existe pas', 404);
        }

        $sql = "
            SELECT * from reservation
            WHERE id_salle = 1 AND
            date = '$date' AND
            id != $id AND
            (
                (
                    heure_debut <= $heure_debut AND
                    heure_fin > $heure_debut
                ) OR (
                    heure_debut < $heure_fin AND
                    heure_fin >= $heure_fin
                )
             )
        ";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $req = $stmt->fetchAll();

        if (empty($req)) {
            $em = $this->getDoctrine()->getManager();

            $reservation->set_date($date_formatted);
            $reservation->set_heure_debut($heure_debut);
            $reservation->set_heure_fin($heure_fin);

            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($reservation);

            // actually executes the queries (i.e. the INSERT query)
            $res = $em->flush();

            $formatted = array (
                'res' => 'success',
                'msg' => 'Réservation mise à jour'
            );
        }else{
            $formatted = array(
                'res' => 'err',
                'msg' => 'Il y a déjà une réservation à cette heure ci'
            );
        }
        return new JsonResponse($formatted);
    }
}
