<?php

namespace AppBundle\Entity;

/**
 * Reservation
 */
class Reservation
{
    /**
     * @var int
     */
    private $id;
    private $id_user;
    private $id_salle;
    private $date;
    private $heure_debut;
    private $heure_fin;

    /*public function __construct($id, $id_user, $id_salle, $date, $heure_debut, $heure_fin)
    {
        $this->id = $id;
        $this->id_user = $id_user;
        $this->id_salle = $id_salle;
        $this->date = $date;
        $this->heure_debut = $heure_debut;
        $this->heure_fin = $heure_fin;
    }*/


    public function get_id()
    {
        return $this->id;
    }

    public function get_user()
    {
        return $this->id_user;
    }

    public function get_salle()
    {
        return $this->id_salle;
    }

    public function get_date()
    {
        return $this->date;
    }

    public function get_heure_debut()
    {
        return $this->heure_debut;
    }

    public function get_heure_fin()
    {
        return $this->heure_fin;
    }

    public function set_id($id)
    {
        $this->id=$id;
    }

    public function set_user($user)
    {
        $this->id_user=$user;
    }

    public function set_salle($salle)
    {
        $this->id_salle=$salle;
    }

    public function set_date($date)
    {
        $this->date=$date;
    }

    public function set_heure_debut($heure_debut)
    {
        $this->heure_debut= $heure_debut;
    }

    public function set_heure_fin($heure_fin)
    {
        $this->heure_fin=$heure_fin;
    }
}

