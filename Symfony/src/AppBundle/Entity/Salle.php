<?php

namespace AppBundle\Entity;

/**
 * Salle
 */
class Salle
{
    /**
     * @var int
     */
    private $id;
    private $num_salle;
    private $nom_salle;

    public function __construct($id, $num_salle, $nom_salle)
    {
        $this->id = $id;
        $this->num_salle = $num_salle;
        $this->nom_salle = $nom_salle;
    }

    public function get_id()
    {
        return $this->id;
    }

    public function get_num_salle()
    {
        return $this->num_salle;
    }

    public function get_nom_salle()
    {
        return $this->nom_salle;
    }
}

