<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($rawPathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // home
        if ('' === $trimmedPathinfo) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_home;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($rawPathinfo.'/', 'home');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\HomeController::home',  '_route' => 'home',);
        }
        not_home:

        if (0 === strpos($pathinfo, '/salles')) {
            // salles_list
            if (preg_match('#^/salles/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_salles_list;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'salles_list')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::getSallesAction',));
            }
            not_salles_list:

            // get_salle
            if (preg_match('#^/salles/(?P<token>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_get_salle;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_salle')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::getSalleAction',));
            }
            not_get_salle:

        }

        // get_dispo
        if (0 === strpos($pathinfo, '/dispo') && preg_match('#^/dispo/(?P<token>[^/]++)/(?P<heure_debut>[^/]++)/(?P<heure_fin>[^/]++)/(?P<date>[^/]++)$#s', $pathinfo, $matches)) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_get_dispo;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_dispo')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::getDispoAction',));
        }
        not_get_dispo:

        if (0 === strpos($pathinfo, '/reserver')) {
            // post_salle
            if (preg_match('#^/reserver/(?P<token>[^/]++)/(?P<id_salle>[^/]++)/(?P<heure_debut>[^/]++)/(?P<nb_heures>[^/]++)/(?P<id_user>[^/]++)/(?P<date>[^/]++)$#s', $pathinfo, $matches)) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_post_salle;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_salle')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::postReservationAction',));
            }
            not_post_salle:

            // put_salle
            if (0 === strpos($pathinfo, '/reserver/edit') && preg_match('#^/reserver/edit/(?P<token>[^/]++)/(?P<id>[^/]++)/(?P<heure_debut>[^/]++)/(?P<nb_heures>[^/]++)/(?P<date>[^/]++)$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_put_salle;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'put_salle')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::updateReservationAction',));
            }
            not_put_salle:

        }

        // delete_reservation
        if (0 === strpos($pathinfo, '/reservation') && preg_match('#^/reservation/(?P<token>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            if ('DELETE' !== $canonicalMethod) {
                $allow[] = 'DELETE';
                goto not_delete_reservation;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_reservation')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::deleteReservationAction',));
        }
        not_delete_reservation:

        // get_salle_planning
        if (0 === strpos($pathinfo, '/planning') && preg_match('#^/planning/(?P<token>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_get_salle_planning;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_salle_planning')), array (  '_controller' => 'AppBundle\\Controller\\SalleController::getSallePlanningAction',));
        }
        not_get_salle_planning:

        // app.swagger_ui
        if ('/api/doc' === $pathinfo) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_appswagger_ui;
            }

            return array (  '_controller' => 'nelmio_api_doc.controller.swagger_ui',  '_route' => 'app.swagger_ui',);
        }
        not_appswagger_ui:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
