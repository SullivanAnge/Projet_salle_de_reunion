function loadResaListenners() {
	$('#planning .card.bg-success').click(function() {
		var vehicule = $('#vehicules').val();
		var date = $(this).attr('data-date');
		var demiJ = $(this).attr('data-demiJ');
		openResaPopup('planning',vehicule,date,demiJ);
		});
	}
	
function validResa() {
	var dateDebut  = $('#ResaPopUp #dateDebut').val() + ' ' + $('#ResaPopUp #jour').attr('val');
	var nbDemiJournees = $('#ResaPopUp #demijournee').val()
	var conducteur = $('#ResaPopUp #conducteur').val()
	var vehicule = $('#ResaPopUp #vehicule').val();
	$.ajax({
		method: "POST",
		url: urlAPIVoiture + "reservations/" + loginToken + "?dateDebut=" + dateDebut + '&nbDemiJournees=' + nbDemiJournees + '&conducteur=' + conducteur + '&vehicule=' + vehicule
		})
	.done(function(data){
		loadPlanning(vehicule,$('#ResaPopUp #dateDebut').val(),null);
		$('#ResaPopUp').modal('hide');
		detectError(String(data),'null');
		});
	}	
	
function openResaPopup(origine,vehicule,date,demiJ) {
	$('#ResaPopUp #vehicule').val(vehicule);
	$('#ResaPopUp #dateDebut').val(date);
	if(demiJ == '0')
		{
		$('#ResaPopUp #jour').val('Matin');
		$('#ResaPopUp #jour').attr('val','00:00:00');
		}
		else
		{
		$('#ResaPopUp #jour').val('Après-midi');
		$('#ResaPopUp #jour').attr('val','12:00:00');
		}
		
	$.ajax({
		method: "GET",
		url: "http://chezmeme.com/sso/api/v1.5/who_is/" + loginToken
		})
	.done(function(data){ 	
		$('#ResaPopUp #utilisateur').val(data.prenom + ' ' + data.nom);
		var idUser = data.id;
		$.ajax({
		method: "GET",
			url: "http://chezmeme.com/sso/api/v1.5/users/" + loginToken
			})
		.done(function(data){ 	
			$('#ResaPopUp #conducteur').html('');
			data.forEach(function(e) {
				if(e.id == idUser)
					{
					$('#ResaPopUp #conducteur').append('<option selected value="' + e.id + '">' + e.prenom + ' ' + e.nom + '</option>');
					}
					else
					{
					$('#ResaPopUp #conducteur').append('<option value="' + e.id + '">' + e.prenom + ' ' + e.nom + '</option>');
					}
				});
			});
		});
	
	var count = 0;
	var bloque = false;
	$('#planning .carteDJ').each(function () {
		if($(this).attr('data-date') > date && !bloque)
			{
			if($(this).attr('data-dispo') == '0')
				{
				bloque = true;
				}
				else
				{
				count++;
				}
			}
		if($(this).attr('data-date') == date && demiJ <= $(this).attr('data-demiJ') && !bloque)
			{
			if($(this).attr('data-dispo') == '0')
				{
				bloque = true;
				}
				else
				{
				count++;
				}
			}
		});
	
	var i = 1;
	$('#ResaPopUp #demijournee').html('');
	while(i <= count)
		{
		$('#ResaPopUp #demijournee').append('<option value="' + i + '">' + i + '</option>');
		i++;
		}
	
	if(origine == 'planning')
		{
		$('#ResaPopUp .modal-footer button:first-of-type').text('Fermer');
		$('#ResaPopUp .modal-footer button:first-of-type').off();
		$('#ResaPopUp .modal-footer button:first-of-type').attr('data-dismiss','modal');
		}
		else
		{
		$('#ResaPopUp .modal-footer button:first-of-type').text('Retour');
		$('#ResaPopUp .modal-footer button:first-of-type').attr('data-dismiss','none');
		$('#ResaPopUp .modal-footer button:first-of-type').click(function () {
			$('#ResaPopUp').modal('hide');
			$('#RecherchePopUp2').modal();
			});
		}
	
	$('#ResaPopUp .modal-footer button:last-of-type').off();
	$('#ResaPopUp .modal-footer button:last-of-type').click(function () {
		validResa();
		});
	
	$('#ResaPopUp').modal();
	}