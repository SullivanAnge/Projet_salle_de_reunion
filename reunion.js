var jour,
	salleCourante = 0,
	API_SSO = 'http://chezmeme.com/sso/api/v1.6/',
	API = 'http://etudiant9.chezmeme.com/symfonyv2/web',
	portal = 'http://etudiant11.chezmeme.com/portail/';

function whoIs() {
	
	testToken().then(function(token) {
		$.ajax({
			url : API_SSO + 'who_is/' + token,
			type : 'GET',
			dataType : 'json', // On désire recevoir du txt
			success : function(code_json, statut){
				if(code_json.ret === 1) {
					$("#panneau_princ").css("display","block");
					$("#txthead").css("display","block");
					$("#id_user").val(code_json.id);
				} else {
					window.location.replace(portal)
				}
			},
			error : function(resultat, statut, erreur){
				var code = resultat.status;
				var msg = resultat.responseText;
			}
		});
	});
}
	
//fonction de test du token, si token inexistant ou accès refusé alors redirection portail
function testToken() {
	
	return new Promise(function(resolve, reject) {
		if (localStorage.getItem('token')) {
			$.get(API_SSO + 'check_access/' + localStorage.getItem('token'))
				.done(
					function(data) {
						if(data.ret === "granted"){
							resolve(localStorage.getItem('token'));
						} else {
							window.location.replace(portal)
						}
					}
				)
				.fail(
					function(data) {
						window.location.replace(portal)
					}
				)
		} else {
			window.location.replace(portal)
		}
	})
}

// réservation d'une salle
function bookRoom(room, begin, length, user, date) {
	
	testToken().then(function(token) {
		$.ajax({
			type: "POST",
			url: API + '/reserver/' + token + '/' + room + '/' + begin + '/' + length + '/' + user + '/' + date,
			dataType: 'json',
			success : function(resultat, code_html, statut){
				Materialize.toast(resultat.msg,4000);
				$('.modal').modal('close');
				loadPlanning(id_salle)
			},
			error : function(resultat, statut, erreur){
				var code = resultat.status;
				var msg = resultat.responseText;
			}
		});
	});
}

// chargement du planning
function loadPlanning(room) {

	$('#contentCal').children().css("display","none");
	$('#calendar' + room).css("display","block");
	
	salleCourante = room;
	
	testToken().then(function(token) {
		$.ajax({
			url : API + '/planning/' + token + '/' + room,
			type : 'GET',
			dataType : 'json', // On désire recevoir du txt
			success : function(code_json, statut, response){
				var reservations = code_json;
				for(var i=0; i< reservations.planning.length; i++){

					var date = reservations.planning[i].date['date'];
					date = date.substring(0,10);
					// TODO si heure.lenght=1 rajouter un 0 devant 
					var heureDebut = reservations.planning[i].heure_debut + ':00:00';
					var heureFin = reservations.planning[i].heure_fin + ':00:00';
					
					var nom = reservations.planning[i].nom;
					var prenom = reservations.planning[i].prenom;
					
					if(heureDebut.length == 7){
					   heureDebut = '0' + heureDebut;
					}
					if(heureFin.lenght==7){
						heureFin = '0' + heureFin;
					}
					
					//colorier l'heure réservée sur le calendrier
					var monEvent = {
							start: date + 'T' + heureDebut, //'2018-02-06T08:00:00',
							end: date + 'T' + heureFin, //'2018-02-06T09:00:00',
							rendering: 'background',
							color: '#999999',
							constraint: "réservé",
							personne: prenom + " " + nom.substring(0,1)
						};
					
					$('#calendar' + room).fullCalendar('renderEvent', monEvent, true);
				}
			},
			error : function(resultat, statut, erreur){
				var code = resultat.status;
				var msg = resultat.responseText;
			}
		});
	});
	
	$('#contentCal').children().css("display","none");
	$('#calendar' + room).css("display","block");
}

// chargement des salles disponibles
function loadRooms(begin, end, date) {
	testToken().then(function(token) {
		
		//faire une requete get qui reçoit des params
		$.ajax({
			url : API + '/dispo/' + token + '/' + begin + '/' + end + '/' + date,
			type : 'GET',
			dataType : 'json', // On désire recevoir du txt
			success : function(code_json, statut, response){ 
				var salles = code_json;
				for(var i=0; i< salles.length; i++){
					j=i+1;
					$('#panneau_salles').append('<a id="' + "s" +salles[i].id + '" data-num-salle="' + salles[i].id + '" class="col s12 m6 offset-m3 blue-grey darken-2 divSalle z-depth-2 waves-effect waves-light btn-large">'+salles[i].num_salle+'</a>');
				}
			},
			error : function(resultat, statut, erreur){
				var code = resultat.status;
				var msg = resultat.responseText;
			}
		})
		
	});
}

function logout() {	

	testToken().then(function(token) {
		$.ajax({
			url : API_SSO + 'logout/' + token,
			type : 'GET',
			dataType : 'json', // On désire recevoir du txt
			success : function(code_json, statut){ 
				localStorage.removeItem('token')
				window.location.replace(portal)
			}
		})
	});
}
    			
$(document).ready(function(){
	
	whoIs();
	
	$('.modal').modal();


	$('.datepicker').pickadate({
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 15, // Creates a dropdown of 15 years to control year,
		today: 'Today',
		clear: 'Clear',
		close: 'Ok',
		closeOnSelect: false, // Close upon selecting a date,
		Duration: '01:00:00',
		format: 'yyyy-mm-dd'

	});
				
	$(window).resize(function() {
		if($(window).width() < 730) {
		   $('.calendar').fullCalendar('changeView', 'agendaDay');
		} else {
			
			$('.calendar').fullCalendar('changeView', 'agendaWeek');
		}
	});

	$('.timepicker').pickatime({
		default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: false, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: 'Clear', // text for clear-button
		canceltext: 'Cancel', // Text for cancel-button
		autoclose: false, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
		aftershow: function(){} //Function for after opening timepicker
	});

	$('#panneau_salles').on('click', 'a', function(e){
		loadPlanning($(this).data('num-salle'))
	});

	$('#calendar1, #calendar2, #calendar3, #calendar4').fullCalendar({
		header:{
			left:'prev',
			center:'title',
			right:'next'
		},
		defaultView : 'agendaWeek',
		minTime:'8:00:00',
		maxTime:'19:00:00',
		height:450,
		locale:'fr',
		weekends: false, // will hide Saturdays and Sundays
		slotDuration : "1:00:00",
		allDaySlot: false,
		selectable: true,
		selectHelper : true,
		eventClick: function(calEvent, jsEvent, view){
			 $(this).css('border-color', 'red');
		},
		dayClick: function(date, jsEvent, view) {
			// var token = testToken(localStorage.getItem('token'))
			$('.modal').modal('open');
			jour = date.format();
		},

		slotDuration: '01:00:00',
		eventRender: function(event, element) {
			element.find(".fc-event-title").remove();
			element.find(".fc-event-time").remove();
			var new_description = event.personne;
			$(this).css({'pointer-events' : 'none'});
			element.append(new_description);
		}
	});

	$('#btnlogout').click(function(e){
		e.preventDefault()
		logout()
	});

	/*Affiche les salles selon la recherche*/

	$("#panneau_princ input").change(function(){
		
		$(".divSalle").remove();
		loadRooms($("#heureDebut").val() + ':00', $("#heureFin").val() + ':00', $("#date").val())
	});

	$("#form").submit(function(e){
		e.preventDefault();
		var id_user = $("#id_user").val(),
			id_salle = salleCourante,
			heure_debut = jour.substring(11),
			date = jour.substring(10,0),
			heures = $("#heures").val();
		bookRoom(id_salle, heure_debut, heures, id_user, date)
	})
});