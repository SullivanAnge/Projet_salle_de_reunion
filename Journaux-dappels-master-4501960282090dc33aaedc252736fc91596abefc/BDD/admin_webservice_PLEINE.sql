-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 08 Février 2018 à 09:34
-- Version du serveur :  10.0.32-MariaDB-0+deb8u1
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `admin_webservice`
--

-- --------------------------------------------------------

--
-- Structure de la table `appel`
--

CREATE TABLE `appel` (
  `i_index` int(11) NOT NULL,
  `d_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `s_objet` varchar(500) DEFAULT NULL,
  `s_commentaire` varchar(500) NOT NULL,
  `i_statut` int(11) NOT NULL COMMENT 'O - N - Rappelé',
  `i_contact` int(11) DEFAULT NULL,
  `i_urgence` int(11) NOT NULL COMMENT 'Basse Moyenne Haute',
  `i_type` int(11) NOT NULL COMMENT 'Info Sav Interne Annulation Rdv',
  `i_personne` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `appel`
--

INSERT INTO `appel` (`i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne`) VALUES
(1, '2018-01-25 12:51:54', 'Premier appel', '', 1, 1, 1, 1, 1),
(2, '2018-01-25 12:51:54', 'Second appel', '', 1, 1, 2, 2, 2),
(3, '2018-01-30 09:11:53', 'Troisieme', '', 2, 1, 3, 4, 2),
(4, '2018-02-06 08:57:15', 'test', '', 2, 1, 1, 1, 1),
(5, '2018-02-06 08:57:15', 'test', '', 2, 1, 1, 1, 1),
(6, '2018-02-06 08:57:15', 'test', '', 2, 1, 1, 1, 1),
(7, '2018-02-06 10:40:00', 'jh', '', 2, 20, 1, 1, 1),
(8, '2018-02-06 09:28:38', 'azazeae', '', 2, 4, 1, 1, 2),
(9, '2018-02-19 18:28:00', 'azertyuiop', '', 2, 21, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `i_index` int(11) NOT NULL,
  `s_name` varchar(50) NOT NULL,
  `s_telephone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`i_index`, `s_name`, `s_telephone`) VALUES
(1, 'Dupont', '0296452789'),
(2, 'De Maupassant', '0607535418'),
(3, 'Dupy', '0696457824'),
(4, 'Vaillant', '0296487568'),
(20, 'azertyuiop', '7852155111'),
(21, 'poiuytreza', '9876543210');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `i_index` int(11) NOT NULL,
  `s_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`i_index`, `s_name`) VALUES
(1, 'Corentin'),
(2, 'Brendan');

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE `statut` (
  `i_index` int(11) NOT NULL,
  `s_statut` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `statut`
--

INSERT INTO `statut` (`i_index`, `s_statut`) VALUES
(1, 'oui'),
(2, 'non');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `s_type` varchar(10) NOT NULL,
  `i_index` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`s_type`, `i_index`) VALUES
('Info', 1),
('Sav', 2),
('Interne', 3),
('Annulation', 4),
('Rdv', 5);

-- --------------------------------------------------------

--
-- Structure de la table `urgence`
--

CREATE TABLE `urgence` (
  `s_urgence` varchar(10) NOT NULL,
  `i_index` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `urgence`
--

INSERT INTO `urgence` (`s_urgence`, `i_index`) VALUES
('Basse', 1),
('Moyenne', 2),
('Haute', 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `appel`
--
ALTER TABLE `appel`
  ADD PRIMARY KEY (`i_index`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`i_index`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`i_index`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`i_index`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`i_index`);

--
-- Index pour la table `urgence`
--
ALTER TABLE `urgence`
  ADD PRIMARY KEY (`i_index`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `appel`
--
ALTER TABLE `appel`
  MODIFY `i_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `i_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `i_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `statut`
--
ALTER TABLE `statut`
  MODIFY `i_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
  MODIFY `i_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `urgence`
--
ALTER TABLE `urgence`
  MODIFY `i_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
