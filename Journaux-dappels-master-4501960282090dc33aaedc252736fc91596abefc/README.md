# Journaux d'appels

Application permettant la gestion des appels d'une entreprise.

Lien vers la documentation de l'API : https://etudiant17.chezmeme.com/Call/web/doc.html

## Getting Started

Ces instructions permettent d'obtenir une copie du projet, pour du développement et des test. 

NOTE : Ce n'est pas la version finale du projet !

## Prerequisites

- PHP 7+
- Symfony
- Composer
- MySQL
- GIT

## Installing

Suivre étape par étape les instructions suivantes : 

Télécharger le répertoire du projet à l'adresse suivante

```
https://gitlab.com/Corentin_Jez/Journaux-dappels.git
```

Il est composé de trois dossiers : "APP", "SYMPHONY" et "BDD"
- APP : Contient les fichiers HTML, JS de l'application
- SYMPHONY : Contient les différentes routes de l'applications
- BDD : Contient la base de données de l'application en deux exemplaires (vide et pleine)

### Symfony
- Uploader le contenu du dossier Symfony sur votre serveur.
- Décompresser l'archive sur votre serveur.
- Exécuter la commande suivante en ciblant le dossier obtenu : 
```
chown -R www-data:www-data NomDuDossier
```
- Aller dans le répertoire
 ```
cd NomDuDossier
```
- Installer le projet et ses dépendances avec la commande suivante : 
 ```
composer install
```
- Suivre les instructions d'installation.
- IT WORKS.

### Base de données
- Dans le dossier "BDD", choisir le fichier que vous souhaitez.
- CTRL+F et rechercher "admin_webservice" et remplacer par le nom souhaité pour votre base de données.
- Copier le contenu du fichier.
- Ce sont les instructions SQL pour créer la structure et éventuellement les données de la base de données.
- A l'aide d'un interpréteur SQL sur votre serveur exécutez les lignes copiées.

### Coté client

- Copier le contenu du dossier et placez le dans votre dossier WWW sur votre serveur.
- Il devrait être accessible via : www.domaine.extention/APP

## Built With

* [Symfony](https://symfony.com/) 
* HTML - CSS - JS
* JQuery

## Authors

* **Corentin JEZEQUEL** - *Client Side*
* **Brendan JULIEN** - *Server Side*
* **Vincent LE BARS** - *Doc APi*



## License

This project is licensed under the MIT License
