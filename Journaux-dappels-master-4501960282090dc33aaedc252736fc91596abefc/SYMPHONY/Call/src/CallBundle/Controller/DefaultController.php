<?php

namespace CallBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/* FUNCTION DE CONNECTION A LA BDD */
function connect() {
	$dsn = 'mysql:dbname=admin_webservice;host=127.0.0.1;charset=UTF8';
	$user = 'admin_webservice';
	$password = 'admin_webservice';
	try {
		$dbh = new \PDO($dsn, $user, $password);
	} catch (PDOException $e) {
		//$rep = 'Connexion échouée : ' . $e->getMessage();
		$rep = "echec";
	}
	return $dbh;
}

function check($token)
    {
        $module = 'appels';
        $tabrep = array();

        $url = 'http://chezmeme.com/sso/api/v1.6/check_access/' . $token . '/' . $module;
        $rep = file_get_contents($url);
        $json = json_decode($rep);

        $tabrep[] = $json;
        $rep = $tabrep[0]->ret;

        if ($rep === 'granted') {
            return $rep;
        } else {
            return $rep;
        }
    }

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('Default:index.html.twig');
    }
	
	public function documentationAction()
    {
        $this->render('CallBundle:Default:API-DOC.html');
    }
	
    public function appelsAction($token)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$appels=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne` FROM `appel` ORDER BY `d_date` DESC';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$appels[]=$row;
		}
		//var_dump($appels);
        	return new JsonResponse($appels);
	} else { return new Response("100"); }
    }
	
	public function appelsDateAction($token,$date)
    {
		
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$appels=[];
		$conn= connect();
		$sql =  "SELECT `i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne` FROM `appel` WHERE `d_date` BETWEEN '".$date."-01 00:00:00.000000' AND '".$date."-31 00:00:00.000000'";
		/*$fd = fopen('/var/www/vhosts/etudiant17.chezmeme.com/logs2/errors.log', 'a+');
		fputs($fd, var_export($date, true)));
		fclose($fd);*/
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$appels[]=$row;
		}
		//var_dump($appels);
        	return new JsonResponse($appels);
	} else return new Response("100");
    }
	
	public function appelsInsertAction($token)
    {
	if(check($token) === 'granted'){
		$fd = fopen('/var/www/vhosts/etudiant17.chezmeme.com/logs2/errors.log', 'a+');
		fputs($fd, var_export($_POST, true));
		fclose($fd);
		$date = $_POST['d_date'];
		$objet = $_POST['s_object'];
		$commentaire = "";
		$statut = $_POST['i_statut'];
		$contact = $_POST['i_contact'];
		$urgence = $_POST['i_urgence'];
		$type = $_POST['i_type'];
		$personne = $_POST['i_personne'];
		
		$dbh = connect();
		$stmt = $dbh->prepare("INSERT INTO appel (`d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne`) VALUES (:date, :objet, :commentaire, :statut, :contact, :urgence, :type, :personne)");
		$stmt->bindParam(':date', $date);
		$stmt->bindParam(':objet', $objet);	
		$stmt->bindParam(':commentaire', $commentaire);	
		$stmt->bindParam(':statut', $statut);	
		$stmt->bindParam(':contact', $contact);	
		$stmt->bindParam(':urgence', $urgence);	
		$stmt->bindParam(':type', $type);	
		$stmt->bindParam(':personne', $personne);	
		
		$stmt->execute();
		//echo "\nPDOStatement::errorInfo():\n";
		//$arr = $dbh->errorInfo();
		//error_log($arr);
		return new Response($date);
	} else return new Response("100");
    }
	
	
	public function appelsInfosAction($token, $id)
    {
		if(check($token) === 'granted'){
		/* Penser a check le token */
		$appels=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne` FROM `appel` WHERE (`i_index`='.$id.') ORDER BY `d_date` DESC';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$appels[]=$row;
		}
		//var_dump($appels);
        return new JsonResponse($appels);
} else return new Response("100");
    }
	public function appelChangeAction($token){
                if(check($token) === 'granted'){
                $index = $_GET['i_index'];
                $commentaire = $_GET['s_commentaire'];
                $statut = $_GET['i_statut'];
                $dbh = connect();
		//UPDATE `appel` SET `i_statut` = '2', `s_commentaire`='efzfe' WHERE `appel`.`i_index` = 1
                $stmt = $dbh->prepare("UPDATE `appel` SET `i_statut` = :statut, `s_commentaire`=:commentaire WHERE `appel`.`i_index` = :index");
                $stmt->bindParam(':index', $index);
                $stmt->bindParam(':statut', $statut);
                $stmt->bindParam(':commentaire', $commentaire);
        
                $stmt->execute();
		return new Response("a");
		} else return new Response("100");
	}	
	public function appelsTriDestinataireAction($token, $id)
    {
		if(check($token) === 'granted'){
		/* Penser a check le token */
		$appels=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne` FROM `appel` WHERE (`i_personne`='.$id.') ORDER BY `d_date` DESC';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$appels[]=$row;
		}
		//var_dump($appels);
      		 return new JsonResponse($appels);
	} else return new Response("100")    ;
}
	
	public function appelsTriContactAction($token, $id)
    {
		if(check($token) === 'granted'){
		/* Penser a check le token */
		$appels=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne` FROM `appel` WHERE (`i_contact`='.$id.') ORDER BY `d_date` DESC';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$appels[]=$row;
		}
		//var_dump($appels);
        return new JsonResponse($appels);
	} else return new Response("100");
    }
	
	public function appelsTriNumeroAction($token, $numero)
    {
	if(check($token) === 'granted'){
		$contacts=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_name`, `s_telephone` FROM `contact` WHERE `s_telephone`='.$numero.' ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$contacts[]=$row;
		}
		$contact = $contacts[0]['i_index'];
		/* Penser a check le token */
		$appels=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `d_date`, `s_objet`, `s_commentaire`, `i_statut`, `i_contact`, `i_urgence`, `i_type`, `i_personne` FROM `appel` WHERE (`i_contact`='.$contact.') ORDER BY `d_date` DESC';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$appels[]=$row;
		}
		//var_dump($appels);
        return new JsonResponse($appels);
	} else return new Response("100");
    }
	
	
	 public function contactsAction($token)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$contacts=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_name`, `s_telephone` FROM `contact` ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$contacts[]=$row;
		}
		//var_dump($contacts);
        return new JsonResponse($contacts);
	} else return new Response("100");
    }
	
	public function contactsInsertAction($token)
    {
	if(check($token) === 'granted'){
		/*$fd = fopen('/var/www/vhosts/etudiant17.chezmeme.com/logs2/errors.log', 'a+');
		fputs($fd, var_export($_POST, true));
		fclose($fd);*/
		$name = $_POST['s_name'];
		$phone = $_POST['s_telephone'];
		
		$dbh = connect();
		$stmt = $dbh->prepare("INSERT INTO contact (`s_name`, `s_telephone`) VALUES (:name, :phone)");
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':phone', $phone);		
		
		$stmt->execute();
		//echo "\nPDOStatement::errorInfo():\n";
		//$arr = $dbh->errorInfo();
		//error_log($arr);
		return new Response($name);
	} else return new Response("100");
    }
	
	 public function contactsInfosAction($token, $id)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$contacts=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_name`, `s_telephone` FROM `contact` WHERE `i_index`='.$id.' ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$contacts[]=$row;
		}
		//var_dump($contacts);
        return new JsonResponse($contacts);
	} else return new Response("100");
    }
	
	
	
	public function urgencesAction($token)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$urgences=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_urgence` FROM `urgence` ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$urgences[]=$row;
		}
		//var_dump($contacts);
        return new JsonResponse($urgences);
	} else return new Response("100");
    }
	
	public function urgencesNomAction($token, $id)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$urgences=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_urgence`  FROM `urgence` WHERE `i_index`='.$id.'  ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$urgences[]=$row;
		}
		//var_dump($contacts);
        return new JsonResponse($urgences);
	} else return new Response("100");
    }
	
	public function personnesAction($token)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$personnes=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_name` FROM `personne` ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$personnes[]=$row;
		}
		//var_dump($contacts);
        return new JsonResponse($personnes);
	} else return new Response("100");    
}
	
	public function personnesInfosAction($token, $id)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$personnes=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_name` FROM `personne` WHERE `i_index`='.$id.' ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$personnes[]=$row;
		}
		//var_dump($contacts);
        return new JsonResponse($personnes);
	} else return new Response("100");
    }
	
	public function typesAction($token)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$types=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_type` FROM `type` ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$types[]=$row;
		}
        return new JsonResponse($types);
	} else return new Response("100");
    }
	
	public function typesNomAction($token, $id)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$types=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_type` FROM `type`  WHERE `i_index`='.$id.' ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$types[]=$row;
		}
        return new JsonResponse($types);
	}  else return new Response("100");
    }
	
	public function statutsAction($token)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$statuts=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_statut` FROM `statut` ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$statuts[]=$row;
		}
        return new JsonResponse($statuts);
	} else return new Response("100");
    }
	
	public function statutsNomAction($token, $id)
    {
	if(check($token) === 'granted'){
		/* Penser a check le token */
		$statuts=[];
		$conn= connect();
		$sql =  'SELECT `i_index`, `s_statut` FROM `statut` WHERE (`i_index`= '.$id.')  ';
		foreach  ($conn->query($sql, \PDO::FETCH_ASSOC) as $row) {
			$statuts[]=$row;
		}
        return new JsonResponse($statuts);
	} else return new Response("100");
    }
	
	

}
